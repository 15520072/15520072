#ifndef GRAPHICS2D_FILLCOLOR_H
#define GRAPHICS2D_FILLCOLOR_H

#include <SDL.h>
#include "Vector2D.h"
#include "Line.h"

struct Node {
	Vector2D vt;
	Node * next;
};

class Stack {
private:
	Node * top;
public:
	Stack()
	{
		this->top = NULL;
	}

	Node * CreateNode(Vector2D vt)
	{
		Node * p = new Node();
		if (p != NULL)
		{
			p->vt = vt;
			p->next = NULL;
		}
		return p;
	}

	bool IsEmpty()
	{
		return this->top == NULL;
	}

	int push(Vector2D vt)
	{
		Node * p = CreateNode(vt);
		if (p == NULL)
			return -1;
		p->next = this->top;
		this->top = p;
		return 0;
	}

	int pop()
	{
		if (this->IsEmpty())
			return -1;
		Node * p = this->top;
		this->top = this->top->next;
		p = NULL;
		return 0;
	}

	Vector2D get()
	{
		if (this->IsEmpty())
			return NULL;
		Vector2D vt(this->top->vt);
		return vt;
	}
};

void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor);
void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor);
void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor);
void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor);
void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor);
void FillIntersectionEllipseCircle(int xcE, int ycE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor);
void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2, SDL_Renderer *ren, SDL_Color fillColor);


#endif

