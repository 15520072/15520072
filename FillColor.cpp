﻿#include "FillColor.h"
#include <iostream>
#include <math.h>
using namespace std;

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
    SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

    Uint32 temp;
    Uint8 red, green, blue, alpha;

    /* Get Red component */
    temp = pixel & fmt->Rmask;  /* Isolate red component */
    temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
    temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
    red = (Uint8)temp;

    /* Get Green component */
    temp = pixel & fmt->Gmask;  /* Isolate green component */
    temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
    temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
    green = (Uint8)temp;

    /* Get Blue component */
    temp = pixel & fmt->Bmask;  /* Isolate blue component */
    temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
    temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
    blue = (Uint8)temp;

    /* Get Alpha component */
    temp = pixel & fmt->Amask;  /* Isolate alpha component */
    temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
    temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
    alpha = (Uint8)temp;

    SDL_Color color = {red, green, blue, alpha};
    return color;

}

//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {
    SDL_Surface* saveSurface = NULL;
    SDL_Surface* infoSurface = NULL;
    infoSurface = SDL_GetWindowSurface(SDLWindow);
    if (infoSurface == NULL) {
        std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
    } else {
        unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
        if (pixels == 0) {
            std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
            return NULL;
        } else {
            if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
                std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
                return NULL;
            } else {
                saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
            }
        }
    }
    return saveSurface;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
    if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
        return true;
    return false;
}

//Check if pixel can be filled
bool canFilled(SDL_Window *win, Vector2D newPoint,Uint32 pixel_format,
               SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
    SDL_Surface *surface = getPixels(win, ren);

    //Convert the pixels to 32 bit
    Uint32 *pixels = (Uint32 *)surface->pixels;
    int w = surface->w;

    int index = newPoint.y * w + newPoint.x;
    Uint32 pixel = pixels[index];
    SDL_Color color = getPixelColor(pixel_format, pixel);
    color.a = 255;

    if (!compareTwoColors(color, fillColor) && !compareTwoColors(color, boundaryColor))
    {
        return true;
    }

    return false;
}

void BoundaryFill4(SDL_Window *win, Vector2D startPoint,Uint32 pixel_format,
                   SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	
	Stack * s = new Stack();
	if (canFilled(win, startPoint, pixel_format, ren, fillColor, boundaryColor))
		s->push(startPoint);
	
	while (!s->IsEmpty())
	{
		Vector2D vt(s->get().x, s->get().y);
		s->pop();
		
		SDL_RenderDrawPoint(ren, vt.x, vt.y);
		Vector2D _vt(vt.x, vt.y + 1);
		if (canFilled(win, _vt, pixel_format, ren, fillColor, boundaryColor))
			s->push(_vt);
		_vt.set(vt.x, vt.y - 1);
		if (canFilled(win, _vt, pixel_format, ren, fillColor, boundaryColor))
			s->push(_vt);
		_vt.set(vt.x + 1, vt.y);
		if (canFilled(win, _vt, pixel_format, ren, fillColor, boundaryColor))
			s->push(_vt);
		_vt.set(vt.x - 1, vt.y);
		if (canFilled(win, _vt, pixel_format, ren, fillColor, boundaryColor))
			s->push(_vt);
	}
}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	int lc = a;
	if (b > lc)
		c = b;
	if (c > lc)
		lc = c;
	return lc;
}

int minIn3(int a, int b, int c)
{
	int lc = a;
	if (b < lc)
		c = b;
	if (c < lc)
		lc = c;
	return lc;
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D temp(a);
	a.set(b);
	b.set(temp);
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
{
	if (v1.y > v2.y) {
		swap(v1, v2);
	}

	if (v1.y > v3.y)
	{
		swap(v1, v3);
	}

	if (v2.y > v3.y)
	{
		swap(v2, v3);
	}
}

void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	int xmin = maxIn3(v1.x, v2.x, v3.x);  
	int xmax = minIn3(v1.y, v2.y, v3.y);
	SDL_RenderDrawLine(ren, xmin, v1.y, xmax, v1.y);
}

void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float flst1 = (float)(v3.x - v1.x) / (v3.y - v3.y);  
	float flst2 = (float)(v3.x - v2.x) / (v3.y - v2.y);  
	float xLeft = v1.x;  
	float xRight = v2.x;  
	for (int y = v1.y; y <= v3.y; y++) 
	{ 
		xLeft += flst1;   
		xRight += flst2;   
		SDL_RenderDrawLine(ren, int(xLeft + 0.5), y, int(xRight + 0.5), y); 
	}
}

void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float flst1 = (float)(v2.x - v1.x) / (v2.y - v1.y); 
	float flst2 = (float)(v3.x - v1.x) / (v3.y - v1.y);  
	float xLeft = v1.x; 
	float xRight = v1.x; 
	for (int y = v1.y; y <= v3.y; y++) 
	{ 
		xLeft += flst1;   
		xRight += flst2;  
		SDL_RenderDrawLine(ren, int(xLeft + 0.5), y, int(xRight + 0.5), y); 
	}
}

void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float flst1 = (float)(v2.x - v1.x) / (v2.y - v1.y); 
	float flst2 = (float)(v3.x - v1.x) / (v3.y - v1.y); 
	float xLeft = v1.x; 
	float xRight = v1.x;
	for (int y = v1.y; y <= v2.y; y++) 
	{ 
		xLeft += flst1;  
		xRight += flst2; 
		SDL_RenderDrawLine(ren, int(xLeft + 0.5), y, int(xRight + 0.5), y);
	}
	flst1 = (float)(v3.x - v2.x) / (v3.y - v2.y);  
	for (int y = v2.y; y <= v3.y; y++) 
	{
		xLeft += flst1;   
		xRight += flst2;
		SDL_RenderDrawLine(ren, int(xLeft + 0.5), y, int(xRight + 0.5), y);
	}
}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	ascendingSort(v1, v2, v3);
	if (v1.y == v2.y && v2.y == v3.y)
	{ 
		TriangleFill1(v1, v2, v3, ren, fillColor);
		return; 
	}  
	if (v1.y == v2.y && v2.y<v3.y) 
	{
		TriangleFill2(v1, v2, v3, ren, fillColor);
		return; 
	}  
	if (v1.y<v2.y && v2.y == v3.y) 
	{ 
		TriangleFill3(v1, v2, v3, ren, fillColor);
		return;
	}  
	else
		TriangleFill3(v1, v2, v3, ren, fillColor);
	return;
}
//======================================================================================================================
//===================================CIRCLE - RECTANGLE - ELLIPSE=======================================================

int Max2(int a, int b)
{
	return a > b ? a : b;
}

int Min2(int a, int b)
{
	return a < b ? a : b;
}

bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	return (pow(x - xc, 2) + pow(y - yc, 2) <= pow(R, 2));
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int xmin = Min2(x1, x2); 
	int xmax = Max2(x1, x2); 
	for (int x = xmin; x <= xmax; x++) 
		if (isInsideCircle(xc, yc, R, x, y1))
			SDL_RenderDrawPoint(ren, x, y1);
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int ymax = Max2(vTopLeft.y, vBottomRight.y);
	int ymin = Min2(vTopLeft.y, vBottomRight.y);
	for (int y = ymin; y <= ymax; y++)
		FillIntersection(vTopLeft.x, y, vBottomRight.x, y, xc, yc, R, ren, fillColor);
}

void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{
	int ymax = Max2(vTopLeft.y, vBottomRight.y);
	int ymin = Min2(vTopLeft.y, vBottomRight.y);
	for (int y = vBottomRight.y; y <= vTopLeft.y; y++)
		SDL_RenderDrawLine(ren, vTopLeft.x, y, vBottomRight.x, y);
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_RenderDrawLine(ren, x + xc, y + yc, -x + xc, y + yc);
	SDL_RenderDrawLine(ren, y + xc, x + yc, -y + xc, x + yc);
	SDL_RenderDrawLine(ren, y + xc, -x + yc, -y + xc, -x + yc);
	SDL_RenderDrawLine(ren, x + xc, -y + yc, -x + xc, -y + yc);
}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = 0, y = R;
	int p = 2 * pow((x + 1), 2) - 2 * pow(R, 2) + pow(y, 2) + pow((y - 1), 2);
	while (x <= y)
	{
		if (p <= 0)
		{
			put4line(xc, yc, x, y, ren, fillColor);
			p += 4 * x + 6;
		}
		else
		{
			put4line(xc, yc, x, y, ren, fillColor);
			p += 4 * (x - y) + 10;
			y--;
		}
		x++;
	}
}

void InputFourLine(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor, int xc2, int yc2, int R2)
{
	FillIntersection(x + xc, y + yc, -x + xc, y + yc, xc2, yc2, R2, ren, fillColor);
	FillIntersection(y + xc, x + yc, -y + xc, x + yc, xc2, yc2, R2, ren, fillColor);
	FillIntersection(y + xc, -x + yc, -y + xc, -x + yc, xc2, yc2, R2, ren, fillColor);
	FillIntersection(x + xc, -y + yc, -x + xc, -y + yc, xc2, yc2, R2, ren, fillColor);
}

void FillIntersectionEllipseCircle(int xcE, int ycE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = 0, y = b;
	int p = -2 * pow(a, 2)*b + pow(a, 2) + 2 * pow(b, 2);
	InputFourLine(xcE, ycE, x, y, ren, fillColor, xc, yc, R);
	while (pow(x, 2)*(pow(a, 2) + pow(b, 2)) <= pow(a, 4))
	{
		if (p <= 0)
		{
			p += 4 * pow(b, 2)*x + 6 * pow(b, 2);
		}
		else
		{
			p+= 4 * pow(b, 2)*x - 4 * pow(a, 2)*y + 4 * pow(a, 2) + 6 * pow(b, 2);
			y--;
		}
		x++;
		InputFourLine(xcE, ycE, x, y, ren, fillColor, xc, yc, R);
	}

	x = a, y = 0;
	p = -2 * a*pow(b, 2) + pow(b, 2) + 2 * pow(a, 2);
	InputFourLine(xcE, ycE, x, y, ren, fillColor, xc, yc, R);
	while (pow(x, 2)*(pow(a, 2) + pow(b, 2)) >= pow(a, 4))
	{
		if (p <= 0)
		{
			p += 4 * pow(a, 2)*y + 6 * pow(a, 2);
		}
		else
		{
			p += 4 * pow(a, 2)*y - 4 * pow(b, 2)*x + 4 * pow(b, 2) + 6 * pow(a, 2);
			x--;
		}
		y++;
		InputFourLine(xcE, ycE, x, y, ren, fillColor, xc, yc, R);
	}
}

void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	//chuyển việc tô phần giao từng đoạn thằng trên đường tròn 1 với đường đường tròn 2
	int x = 0, y = R1;
	int p = 2 * pow((x + 1), 2) - 2 * pow(R1, 2) + pow(y, 2) + pow((y - 1), 2);
	InputFourLine(xc1, yc1, x, y, ren, fillColor, xc2, yc2, R2);
	while (x <= y)
	{
		if (p <= 0)
		{
			p += 4 * x + 6;
		}
		else
		{
			p += 4 * (x - y) + 10;
			y--;
		}
		x++;
		InputFourLine(xc1, yc1, x, y, ren, fillColor, xc2, yc2, R2);
	}
}

