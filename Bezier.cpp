﻿#include "Bezier.h"
#include <math.h>
#include <iostream>
using namespace std;

void DrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0, y = R;
	int p = 2 * pow((x + 1), 2) - 2 * pow(R, 2) + pow(y, 2) + pow((y - 1), 2);
	while (x <= y)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		SDL_RenderDrawPoint(ren, xc - x, yc + y);
		SDL_RenderDrawPoint(ren, xc + y, yc + x);
		SDL_RenderDrawPoint(ren, xc - y, yc + x);
		SDL_RenderDrawPoint(ren, xc + y, yc - x);
		SDL_RenderDrawPoint(ren, xc - y, yc - x);
		SDL_RenderDrawPoint(ren, xc + x, yc - y);
		SDL_RenderDrawPoint(ren, xc - x, yc - y);
	}
}

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	double t = 0;
	for (t; t <= 1; t += 0.001)
	{
		int x = pow(1 - t, 2) * p1.x + t * t * p2.x + (2 * t - 2 * t * t) * p3.x,
			y = pow(1 - t, 2) * p1.y + t * t * p2.y + (2 * t - 2 * t * t) * p3.y;
		SDL_RenderDrawPoint(ren, x, y);
	}
}

void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	double t = 0;
	for (t; t <= 1; t += 0.001)
	{
		int x = pow(1 - t, 3) * p1.x + t * t * t * p2.x + 3 * t * pow(1 - t, 2) * p3.x + 3 * pow(t, 2) * (1 - t) * p4.y,
			y = pow(1 - t, 3) * p1.y + t * t * t * p2.y + 3 * t * pow(1 - t, 2) * p3.y + 3 * pow(t, 2) * (1 - t) * p4.y;
		SDL_RenderDrawPoint(ren, x, y);
	}
}

void Draw(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4, SDL_Color colorCurve, SDL_Color colorRect)
{
	SDL_SetRenderDrawColor(ren, colorCurve.a, colorCurve.g, colorCurve.b, 255);
	//vẽ đường nối các điểm
	SDL_RenderDrawLine(ren, p1.x, p1.y, p2.x, p2.y);
	SDL_RenderDrawLine(ren, p2.x, p2.y, p4.x, p4.y);
	SDL_RenderDrawLine(ren, p3.x, p3.y, p4.x, p4.y);
	SDL_RenderDrawLine(ren, p3.x, p3.y, p1.x, p1.y);

	SDL_SetRenderDrawColor(ren, colorRect.a, colorRect.g, colorRect.b, 255);
	//thực hiện vẽ đường cong bezier
	DrawCircle(p1.x, p1.y, 3, ren);
	DrawCircle(p2.x, p2.y, 3, ren);
	DrawCircle(p3.x, p3.y, 3, ren);
	DrawCircle(p4.x, p4.y, 3, ren);

	SDL_SetRenderDrawColor(ren, 36, 113, 163, 255);
	DrawCurve2(ren, p1, p2, p3);
	DrawCurve3(ren, p1, p2, p3, p4);
}
