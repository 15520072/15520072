﻿#include <iostream>
#include <SDL.h>
#include "Bezier.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);


	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 3;
	rect1->y = p1.y - 3;
	rect1->w = 6;
	rect1->h = 6;

	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 3;
	rect2->y = p2.y - 3;
	rect2->w = 6;
	rect2->h = 6;

	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 3;
	rect3->y = p3.y - 3;
	rect3->w = 6;
	rect3->h = 6;

	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 3;
	rect4->y = p4.y - 3;
	rect4->w = 6;
	rect4->h = 6;

	SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };

	Vector2D p1_temp(p1), //biến lưu trữ giá trị cũ của điểm p1, p2, p3, p4
		p2_temp(p2),
		p3_temp(p3), 
		p4_temp(p4); 
	//Take a quick break after all that hard work
	//Quit if happen QUIT event

	Draw(ren, p1, p2, p3, p4, colorCurve, colorRect);

	SDL_RenderPresent(ren);

	bool running = true; 
	bool p1_enable = false, //biến kiểm tra cho phép điểm di chuyển p1, p2, p3, p4
		p2_enable = false,
		p3_enable = false, 
		p4_enable = false;

	while (running)
	{
		SDL_Event event;
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			//HANDLE MOUSE EVENTS!!!


			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false; 
			}
			else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) //khi chuột trái được bấm
			{
				if (pow(event.button.x - p1.x, 2) + pow(event.button.y - p1.y, 2) <= 9)
					p1_enable = true;
				else if (pow(event.button.x - p2.x, 2) + pow(event.button.y - p2.y, 2) <= 9)
					p2_enable = true;
				else if (pow(event.button.x - p3.x, 2) + pow(event.button.y - p3.y, 2) <= 9) //nếu chuột ở vị trí điểm p3 thì cho phép di chuyển điểm p3
					p3_enable = true;
				else if (pow(event.button.x - p4.x, 2) + pow(event.button.y - p4.y, 2) <= 9) //nếu chuột ở vị trí điểm p4 thì cho phép di chuyển điểm p4
					p4_enable = true;
			}
			else if (event.type == SDL_MOUSEBUTTONUP) //nếu nhả chuột thì không cho di chuyển điểm kiểm soát
			{
				p1_enable = false;
				p2_enable = false;
				p3_enable = false;
				p4_enable = false;
			}
			
			//di chuyển điểm kiểm soát và vẽ lại đường cong bezier
			if (p1_enable && (pow(event.button.x - p1_temp.x, 2) + pow(event.button.y - p1_temp.y, 2)) > 9) //chuột trái được bấm và vị trị đã ở ngoài điểm ban đầu
			{
				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);
				p1.set(event.button.x, event.button.y);
				p1_temp.set(p1);

				Draw(ren, p1, p2, p3, p4, colorCurve, colorRect);
			}
			else if (p2_enable && (pow(event.button.x - p2_temp.x, 2) + pow(event.button.y - p2_temp.y, 2)) > 9)
			{
				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);
				p2.set(event.button.x, event.button.y);
				p2_temp.set(p2);

				Draw(ren, p1, p2, p3, p4, colorCurve, colorRect);
			}
			else if (p3_enable && (pow(event.button.x - p3_temp.x, 2) + pow(event.button.y - p3_temp.y, 2)) > 9)
			{
				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);
				p3.set(event.button.x, event.button.y);
				p3_temp.set(p3); 

				Draw(ren, p1, p2, p3, p4, colorCurve, colorRect);
			}
			else if (p4_enable && (pow(event.button.x - p3_temp.x, 2) + pow(event.button.y - p3_temp.y, 2)) > 9)
			{
				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);
				p4.set(event.button.x, event.button.y);
				p4_temp.set(p4); 

				Draw(ren, p1, p2, p3, p4, colorCurve, colorRect);
			}

			SDL_UpdateWindowSurface(win);
		}
	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
